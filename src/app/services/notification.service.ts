import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastService: ToastrService) { }

//   This function will show the success messages
  showSuccessMessage(data: any) {
    this.toastService.success(data.message, "Success :", { closeButton: true , timeOut: 3000 });
  }

//   This function will show the error messages based on the status codes.
  showErrorMessage(data: any) {
    switch (data.code) {
      case 401:
        this.toastService.error(data.message, "Error :", { closeButton: true, timeOut: 3000 });
        break;
      case 409:
        this.toastService.info(data.message, "Info :", { closeButton: true, timeOut: 3000 });
        break;
      case 422:
        this.toastService.error(data.message, "Error :", { closeButton: true, timeOut: 3000 });
        break;
      default:
        this.toastService.error(data.message, "Error :", { closeButton: true, timeOut: 3000 });
        break;
    }
  }
}
